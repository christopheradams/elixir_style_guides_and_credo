## {{ site.title }}

[christopheradams.io](https://christopheradams.io)

---

## Elixir Style Guides


* [christopheradams/elixir_style_guide][christopheradams] - A community-driven
  style guide for Elixir. ([elixirtw/elixir_style_guide][elixirtw])
* [lexmag/elixir-style-guide][lexmag] - An opinionated style guide by Elixir core
  team members.
* [rrrene/elixir-style-guide][rrrene] - Style guide checked by Credo.

[christopheradams]: https://github.com/christopheradams/elixir_style_guide
[elixirtw]: https://github.com/elixirtw/elixir_style_guide
[lexmag]: https://github.com/lexmag/elixir-style-guide
[rrrene]: https://github.com/rrrene/elixir-style-guide

---

## Credo

A static code analysis tool for the Elixir language with a focus on code
consistency and teaching.

Like **RuboCop** and **Inch** but for Elixir.

[credo-ci.org][Credo]

*Credo checks five main style categories...*

[Credo]: http://credo-ci.org/

---

## Credo Checks

* [Software Design](): duplicated code, alias usage, left-over FIXME tags
* [Code Readability](): documentation, snake case, line length
* [Refactor Opportunities](): function size/complexity, conditionals
* [Warnings](): unused values, redefined variables, left-over debugging calls
* [Consistency](): mixed line endings, spacing, etc.

*For example...*

---

## Credo Example

```elixir
unless !allowed? do
  proceed_as_planned
end
```

*The error is...?*


`Unless blocks should not contain a negated condition.` {% fragment %}

```elixir
if allowed? do
  proceed_as_planned
end
```
{% fragment %}

---

## Credo Dependency

```elixir
# mix.exs
defp deps do
  [
    {:credo, "~> 0.5", only: [:dev, :test]}
  ]
end
```

---

## Credo Standalone

```bash
git clone git@github.com:rrrene/credo.git
cd credo
mix deps.get
mix archive.build
mix archive.install
```

```bash
git clone https://github.com/rrrene/bunt
cd bunt
mix archive.build
mix archive.install
```

*This makes it easy to check third-party libraries without adding Credo as a dependency*

---

## Credo Demo

```bash
git clone https://github.com/elixir-lang/gen_stage.git
cd gen_stage
mix credo
```

```
Analysis took 44.9 seconds (0.06s to load, 44.9s running checks)
177 mods/funs, found 3 consistency issues, 4 warnings, 56 refactoring opportunities, 78 code readability issues, 3 software design suggestions.
```

Show issues for one category:

```bash
mix credo --only consistency
```

```
Analysis took 1.9 seconds (0.06s to load, 1.9s running checks)
177 mods/funs, found 3 consistency issues.
```

*Let's fix the errors*

---

## Credo Configuration

```bash
$ mix credo gen.config
* creating .credo.exs
```

---

The End
